var sum = 0;
var count = 0;
while (sum < 10000) {
  sum += ++count;
  //   console.log({ count, sum });
}
document.getElementById(
  "result1"
).innerHTML = `Số nguyên dương nhỏ nhất: ${count}`;

// ex2
function ex2() {
  var x = document.getElementById("txt-num-x")?.value * 1;
  var n = document.getElementById("txt-num-n")?.value * 1;
  var sum = 0;
  for (let i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
    // console.log({ sum });
  }
  //   console.log({ sum });

  document.getElementById("result2").innerHTML = `Tổng: ${sum}`;
}

document.getElementById("btn-ex2").addEventListener("click", ex2);

//ex 3
function ex3() {
  var n = document.getElementById("txt-num-n-3")?.value * 1;
  var factorial = 1;
  for (let i = 1; i <= n; i++) {
    factorial *= i;

    console.log({ factorial });
  }
  //   console.log({ sum });

  document.getElementById("result3").innerHTML = `Giai thừa: ${factorial}`;
}

document.getElementById("btn-ex3").addEventListener("click", ex3);

//ex 4
function ex4() {
  var result = "";
  for (let i = 1; i <= 10; i++) {
    if (i % 2 === 0) {
      result += `<p class="aler alert-danger">Chẵn: ${i}</p>`;
    } else {
      result += `<p class="aler alert-info">Lẻ: ${i}</p>`;
    }
  }
  //   console.log({ sum });

  document.getElementById("result4").innerHTML = result;
}

document.getElementById("btn-ex4").addEventListener("click", ex4);
